package y.main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import y.ipo.IpoEngine;
import y.ipo.Parameter;
import y.ipo.TestGenProfile;

public class TestSetGenerator {

	private static final String UTF_8 = "UTF-8";
	private static final String RESOURCE_OUTPUT_TXT = "resource/output.txt";
	private static final String RESOURCE_INPUT_TXT = "resource/input.txt";

	private static final Logger logger = Logger
			.getLogger(TestSetGenerator.class);

	private static Scanner scanner;
	private static int n;
	private static int b;
	private static Integer[] a;

	public static void main(String... args) {

		try {

			scanner = new Scanner(new File(RESOURCE_INPUT_TXT));

			n = scanner.nextInt();
			logger.info("���������� ������ n=" + n);
			a = new Integer[n];
			for (int i = 0; i < n; i++) {
				a[i] = scanner.nextInt();
			}

			logger.info("��������� ������:" + Arrays.toString(a));
			b = scanner.nextInt();
			logger.info("b=" + b);

			List<Parameter> parameterList = generateParamList();

			logger.info("��������������� �����:"
					+ Arrays.toString(parameterList.toArray()));

			TestGenProfile.instance().setDOI(2);

			IpoEngine ipo = new IpoEngine(parameterList, b);
			ipo.build();

			Files.write(Paths.get(RESOURCE_OUTPUT_TXT), ipo.getTestSet()
					.toStringList(b), Charset.forName(UTF_8));

			logger.info(ipo.getTestSet());

		} catch (IOException e) {
			logger.error("������ �����-����� � ������� " + RESOURCE_INPUT_TXT
					+ " " + RESOURCE_OUTPUT_TXT, e);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	public static List<Parameter> generateParamList() {
		return Parameter.generateParamList(a, n);
	}

}
