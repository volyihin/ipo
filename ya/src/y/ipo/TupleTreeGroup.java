package y.ipo;

import java.util.ArrayList;
import java.util.List;

/**
 * ������ ��� ������� ��������
 */
public class TupleTreeGroup {
	/**
	 * ��������� ����������
	 */
	private List<Parameter> params;
	/**
	 * ������ �������� ��� ������ ����������
	 */
	private TupleTree[] trees;

	private int doi;

	private int exclude;

	/**
	 * ������ ������ � ������������� ��������
	 */
	private int firstMissingTree;

	/**
	 * ���������� �������� �� ��������
	 */
	private int[] missingCounts;
	/**
	 * ������� ������
	 */
	private int currTree;

	public TupleTreeGroup(List<Parameter> params, int index, int exclude) {
		this.params = new ArrayList<Parameter>();
		for (int i = 0; i <= index; i++) {
			this.params.add(params.get(i));
		}

		this.exclude = exclude;

		init();
	}

	public TupleTreeGroup(List<Parameter> params, int index) {
		this(params, index, -1);
	}

	public int getNumOfTrees() {
		return trees.length;
	}

	private void init() {
		doi = TestGenProfile.instance().getDOI();
		int numOfTrees = Combinatorics.nOutOfM(params.size() - 1, doi - 1);

		trees = new TupleTree[numOfTrees];

		firstMissingTree = 0;
		currTree = 0;

		Parameter param = (Parameter) params.get(params.size() - 1);
		missingCounts = new int[param.getDomainSize()];
	}

	public void build() {
		// ���������� ��������� ���������� ����������
		List<Integer[]> paramCombos = null;
		if (exclude == -1) {
			paramCombos = Combinatorics.getParamCombos(params.size() - 1,
					doi - 1);
		} else {
			paramCombos = Combinatorics.getParamCombos(params.size() - 1,
					doi - 1, exclude);
		}

		int totalCountOfTuples = 0;

		for (int j = 0; j < paramCombos.size(); j++) {
			Integer[] paramCombo = (Integer[]) paramCombos.get(j);
			List<Parameter> group = new ArrayList<Parameter>(doi);
			for (int i = 0; i < paramCombo.length; i++) {
				if (paramCombo[i] == 1) {
					group.add(this.params.get(i));
				}
			}
			int index = Combinatorics.getIndex(group, params.size() - 1);

			group.add(params.get(params.size() - 1));

			totalCountOfTuples += getTotalCountOfTuples(group);

			TupleTree tree = new TupleTree(group);
			trees[index] = tree;
		}

		for (int i = 0; i < missingCounts.length; i++) {
			missingCounts[i] = totalCountOfTuples;
		}

	}

	/**
	 * ������ �� ������ ��� ���
	 */
	public boolean isCovered(Tuple tuple) {
		int index = getIndex(tuple);
		return trees[index].lookup(tuple) == null;
	}

	/**
	 * ������������� ������, ��� �������
	 */
	public boolean setCovered(Tuple tuple) {
		boolean rval = false;
		int index = getIndex(tuple);
		if (trees[index].setCovered(tuple)) {
			PVPair pair = tuple.getPair(tuple.getNumOfPairs() - 1);
			missingCounts[pair.value]--;
			rval = true;
		}
		return rval;
	}

	/**
	 * ��������� ���������� ������
	 */
	public Tuple getNextMissingTuple() {
		Tuple rval = null;
		while (firstMissingTree < trees.length) {
			if (trees[firstMissingTree] != null) {
				rval = trees[firstMissingTree].getNextMissingTuple();
			}
			if (rval == null) {
				firstMissingTree++;
			} else {
				break;
			}
		}
		return rval;
	}

	/**
	 * ��������� ��������� ������
	 */
	public void coverNextMissingTuple() {
		while (trees[firstMissingTree] == null) {
			firstMissingTree++;
		}
		trees[firstMissingTree].coverNextMissingTuple();
		if (trees[firstMissingTree].getNextMissingTuple() == null) {
			firstMissingTree++;
		}
	}

	public void initializeIterator() {
		currTree = 0;
		for (int i = 0; i < trees.length; i++) {
			trees[i].initializeIterator();
		}
	}

	public Tuple nextTuple() {
		Tuple rval = null;
		while (currTree < trees.length
				&& (rval = trees[currTree].nextTuple()) == null) {
			currTree++;
		}
		return rval;
	}

	/**
	 * ��������� ������� �� ������� �� ���� ��������
	 */
	public Tuple isCovered() {
		Tuple rval = null;
		for (int i = 0; i < trees.length; i++) {
			if (trees[i] != null) {
				rval = trees[i].isCovered();
			}
			if (rval != null) {
				break;
			}
		}
		return rval;
	}

	private int getIndex(Tuple tuple) {
		List<Parameter> group = new ArrayList<Parameter>(tuple.getNumOfPairs());
		for (int i = 0; i < tuple.getNumOfPairs() - 1; i++) {
			group.add(tuple.getPair(i).param);
		}

		return Combinatorics.getIndex(group, params.size() - 1);
	}

	private int getTotalCountOfTuples(List<Parameter> params) {
		int rval = 1;
		for (int i = 0; i < params.size(); i++) {
			Parameter param = (Parameter) params.get(i);
			rval *= param.getDomainSize();
		}
		return rval;
	}

	public int getCountOfMissingTuples(int value) {
		return missingCounts[value];
	}

	public int getValueWithMostMissingTuples() {
		int rval = TestSet.DONT_CARE;
		int max = 0;
		for (int i = 0; i < missingCounts.length; i++) {
			if (max < missingCounts[i]) {
				max = missingCounts[i];
				rval = i;
			}
		}

		return rval;
	}

	public String toString() {
		StringBuffer rval = new StringBuffer();
		for (int i = 0; i < trees.length; i++) {
			rval.append(trees[i]).append("\n");
		}
		return rval.toString();
	}
}
