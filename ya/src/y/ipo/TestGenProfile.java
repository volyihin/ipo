package y.ipo;

public class TestGenProfile {

	private int doi;

	private static TestGenProfile profile = null;

	private TestGenProfile() {
	}

	public static TestGenProfile instance() {
		if (profile == null) {
			profile = new TestGenProfile();
			profile.doi = 2;
		}

		return profile;
	}

	public int getDOI() {
		return doi;
	}

	public void setDOI(int doi) {
		this.doi = doi;
	}

}
