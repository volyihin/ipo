package y.ipo;

import java.util.List;

import org.apache.log4j.Logger;

import static com.google.common.collect.Lists.*;

/**
 * ���������� ��������� In-Parameter-Order ��� ��������� ����������� ��������
 * �������
 * 
 */
public class IpoEngine {

	private static final Logger logger = Logger.getLogger(IpoEngine.class);
	/**
	 * ������ ����������
	 */
	private List<Parameter> params;

	/**
	 * �������� ������
	 */
	private TestSet ts;

	/**
	 * ������� ��������������, ��� ������������� �����,�������� ����������
	 */
	private int doi;

	private int b;

	private final static int HISTORYSIZE = 2;

	public IpoEngine(List<Parameter> params, int b) {
		this.params = params;
		this.ts = new TestSet(params);
		this.doi = TestGenProfile.instance().getDOI();
		this.b = b;

		// ����� �������� � id
		for (int i = 0; i < params.size(); i++) {
			params.get(i).setID(i);
		}

	}

	public void build() {
		int numOfCoveredParams = 0;

		ts.addMatrix(buildInitialMatrix());
		numOfCoveredParams = params.size() < doi ? params.size() : doi;

		int resource = 0;

		// ��������� �������� ������ ���� �� �����
		for (int column = numOfCoveredParams; column < ts.getNumOfParams(); column++) {

			// ������ ������ �������� ������������ �������� �������
			TupleTreeGroup trees = new TupleTreeGroup(ts.getParams(), column,
					-1);

			trees.build();

			// ���� �� �����������
			expand(column, trees);

			resource = ts.getMatrix().size();

			logger.debug("���������� �������� ������� = " + resource);

			// ���� �� ���������
			grow(column, trees);

			if (resource >= b)
				break;

			logger.debug(trees.toString());
		}

		// ������� �� �������� ������
		carefy();

	}

	/**
	 * ������ ������� ������ ��������� ��� ������ doi ����������
	 */
	private List<Integer[]> buildInitialMatrix() {
		List<Integer[]> rval = newArrayList();

		int numOfParams = params.size() < doi ? params.size() : doi;
		List<Parameter> initialParams = newArrayListWithCapacity(numOfParams);
		for (int i = 0; i < numOfParams; i++) {
			initialParams.add(ts.getParam(i));
		}

		// ������� ���������� ����������
		List<Integer[]> matrix = Combinatorics.getValueCombos(initialParams);
		for (int j = 0; j < matrix.size(); j++) {
			Integer[] row = (Integer[]) matrix.get(j);
			rval.add(row);
		}

		return rval;
	}

	/**
	 * ���� �� �����������. ��������� ������� ����������� ������ ���������.
	 */
	private void expand(int column, TupleTreeGroup trees) {

		Parameter param = ts.getParam(column);
		int domainSize = param.getDomainSize();

		/*
		 * ������� ���������� ��������� ������� ��������, ������������ ���
		 * ������ ����� ����������� � ���������� �����
		 */
		int[] appearances = new int[domainSize];
		// �������
		CyclicArray[] history = new CyclicArray[domainSize];

		for (int i = 0; i < domainSize; i++) {
			appearances[i] = 0;
			history[i] = new CyclicArray(HISTORYSIZE);
		}

		// ��������� �� ������ �������� � ������� �����
		for (int row = 0; row < ts.getNumOfTests(); row++) {
			boolean hasDontCares = hasDontCares(row, column);

			int choice = TestSet.DONT_CARE;
			int maxWeight = -1;
			List<Tuple> maxTuples = null;

			// ���� ��� ������� ��� �������, �� ������� �� �����
			if (trees.getNextMissingTuple() == null) {
				break;
			}

			Integer[] test = ts.getTest(row);

			// ���������� �� ���� ��������� ��������� � ��������� �������� �
			// ������������ �����
			for (int value = 0; value < param.getDomainSize(); value++) {
				if (trees.getCountOfMissingTuples(value) > 0) {
					int[] copy = new int[test.length];
					for (int i = 0; i < test.length; i++) {
						copy[i] = test[i];
					}
					copy[column] = value;

					int currWeight;
					List<Tuple> currTuples = null;

					currTuples = getCoveredTuples(row, column, value, trees,
							history[value], hasDontCares);
					currWeight = currTuples.size();

					// ��������� ������������
					if ((currWeight > maxWeight)
							|| (doi <= 4 && choice != TestSet.DONT_CARE
									&& currWeight == maxWeight && appearances[value] < appearances[choice])) {
						choice = value;
						maxWeight = currWeight;
						maxTuples = currTuples;
					}
				}
			}

			if (choice != TestSet.DONT_CARE) {
				// ������������� �������� choice
				ts.setValue(row, column, choice);

				// �������� �������, �������� ���� �������� �������
				setCovered(row, maxTuples, trees, hasDontCares);

				// book keeping
				appearances[choice]++;
				history[choice].add(row);
			}

		}
	}

	/**
	 * �������� �������, �������� �������� �������
	 */
	private List<Tuple> getCoveredTuples(int row, int column, int value,
			TupleTreeGroup trees, CyclicArray history, boolean hasDontCares) {

		List<Tuple> rval = newArrayList();

		// ��������� ������� ����������
		int[] order = new int[column];
		List<Integer[]> paramCombos = null;

		// ������� � ������� ������� �� ������� ������
		int minDiff = Integer.MAX_VALUE;
		int alike = -1;
		int size = history.size();
		for (int i = 0; i < size; i++) {
			int numOfDiffs = ts
					.getNumOfDifferences(row, history.get(i), column);
			if (numOfDiffs < minDiff) {
				minDiff = numOfDiffs;
				alike = history.get(i);
			}
		}
		// ������������� ��������� �� ���� ��� ����������� ���������� ����������
		int diffIndex = 0;
		int sameIndex = minDiff;

		if (alike != -1 && (column - minDiff) > doi - 1) {
			// ������������� ��������� � � ���������� �������� �������� ������
			for (int i = 0; i < column; i++) {
				if (ts.getValue(row, i) == ts.getValue(alike, i)) {
					order[i] = sameIndex++;
				} else {
					order[i] = diffIndex++;
				}
			}

			// ���������� ����������
			paramCombos = Combinatorics.getParamCombos(column, doi - 1,
					minDiff - 1);
		} else {
			for (int i = 0; i < column; i++) {
				order[i] = i;
			}

			paramCombos = Combinatorics.getParamCombos(column, doi - 1);
		}

		int[] currTest = ts.clone(row);
		// ������� ���������� ��������, ������� ������� ������� ����������
		for (int j = 0; j < paramCombos.size(); j++) {
			Integer[] combo = (Integer[]) paramCombos.get(j);
			Tuple tuple = new Tuple();

			boolean dontCares = false;
			// ������ ������ �� ����������
			for (int i = 0; i < column; i++) {
				if (combo[order[i]] == 1) {
					if (currTest[i] == TestSet.DONT_CARE) {
						dontCares = true;
						break;
					} else {
						tuple.addPair(new PVPair(ts.getParam(i), currTest[i]));
					}
				}
			}

			if (!dontCares) {
				tuple.addPair(new PVPair(ts.getParam(column), value));

				if (!trees.isCovered(tuple)) {
					rval.add(tuple);
				}
			}
		}

		return rval;
	}

	/**
	 * ������������� ������ ��� �������� �� ���� ��������
	 */
	private void setCovered(int row, List<Tuple> tuples, TupleTreeGroup trees,
			boolean hasDontCares) {
		for (int i = 0; i < tuples.size(); i++) {
			Tuple tuple = (Tuple) tuples.get(i);
			trees.setCovered(tuple);

		}
	}

	/**
	 * ����� �� �� �������� �������� ������
	 */
	private boolean hasDontCares(int row, int column) {
		boolean rval = false;
		for (int i = 0; i < column; i++) {
			if (ts.getValue(row, i) == TestSet.DONT_CARE) {
				rval = true;
				break;
			}
		}
		return rval;
	}

	/**
	 * ���� �� ���������
	 */
	private void grow(int column, TupleTreeGroup trees) {
		Tuple tuple = null;

		while ((tuple = trees.getNextMissingTuple()) != null) {
			boolean covered = false;
			for (int row = 0; row < ts.getNumOfTests(); row++) {
				if (ts.isCompatible(row, tuple)) {
					ts.cover(row, tuple);
					covered = true;
					break;
				}
			}
			if (!covered) {
				Integer[] newTest = ts.createNewTest(tuple);
				ts.add(newTest);
			}

			// �� ��������� ������
			trees.coverNextMissingTuple();

		}
	}

	/**
	 * �������� �� ������� �� �������� �������� �������
	 */
	private void carefy() {
		for (int i = 0; i < ts.getNumOfTests(); i++) {
			Integer[] test = ts.getTest(i);

			for (int j = 0; j < test.length; j++) {
				if (test[j] == TestSet.DONT_CARE) {
					Parameter param = ts.getParam(j);
					for (int k = 0; k < param.getDomainSize(); k++) {
						ts.setValue(i, j, k);

					}

				}
			}
		}
	}

	/**
	 * �������� ��� �������� ������
	 */
	public TestSet getTestSet() {
		return ts;
	}

}
