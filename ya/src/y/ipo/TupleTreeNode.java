package y.ipo;

/**
 * ���� ������ ��������
 */
public class TupleTreeNode {
	private int value;
	private TupleTreeNode dad;
	private TupleTreeNode[] kids;

	public TupleTreeNode[] getKids() {
		return kids;
	}

	public TupleTreeNode(int value, int numOfKids) {
		this.value = value;
		kids = new TupleTreeNode[numOfKids];
	}

	public TupleTreeNode(int value) {
		this.value = value;
		kids = null;
	}

	public int getValue() {
		return value;
	}

	public void setDad(TupleTreeNode dad) {
		this.dad = dad;
	}

	public TupleTreeNode getDad() {
		return dad;
	}

	public void setKid(int index, TupleTreeNode kid) {
		kids[index] = kid;
	}

	public TupleTreeNode getKid(int index) {
		return kids[index];
	}

	public int getNumOfKids() {
		return kids.length;
	}

	@Override
	public String toString() {
		return "Node (" + value + ") ";
	}
}
