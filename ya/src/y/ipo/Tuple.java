package y.ipo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ������, ������ ������ ��� : �������� - ��������
 * 
 */
public class Tuple {
	private List<PVPair> pairs;
	/**
	 * ����������� �������, � � ����, ������,..
	 */
	private int dimension;

	/**
	 * ������� ������, ����������� ����� �� �������
	 */
	public Tuple() {
		this.dimension = TestGenProfile.instance().getDOI();
		pairs = new ArrayList<PVPair>(dimension);
	}

	/**
	 * ������ � �������� ������������
	 */
	public Tuple(int dimension) {
		this.dimension = dimension;
		pairs = new ArrayList<PVPair>(dimension);
	}

	/**
	 * �������� ����/������ � � ������
	 */
	public void addPair(PVPair pair) {
		if (pairs.size() == dimension) {
			System.err.println("Tuple Overflows!");
		}
		int i = pairs.size();
		for (; i > 0; i--) {
			PVPair tmp = (PVPair) pairs.get(i - 1);
			if (pair.param.getID() > tmp.param.getID()) {
				break;
			}
		}
		pairs.add(i, pair);
	}

	/**
	 * �������� �� ������� ���������� ��������
	 */
	public boolean hasDontCares() {
		boolean rval = false;
		for (int i = 0; i < pairs.size(); i++) {
			PVPair pair = (PVPair) pairs.get(i);
			if (pair.value == TestSet.DONT_CARE) {
				rval = true;
				break;
			}
		}
		return rval;
	}

	public PVPair getPair(int index) {
		return (PVPair) pairs.get(index);
	}

	public int getNumOfPairs() {
		return pairs.size();
	}

	public Iterator<PVPair> getIterator() {
		return pairs.iterator();
	}

	public String toString() {
		StringBuffer rval = new StringBuffer();
		rval.append("[");
		Iterator<PVPair> it = pairs.iterator();
		boolean first = true;
		while (it.hasNext()) {
			if (!first) {
				rval.append(", ");
			} else {
				first = false;
			}
			rval.append(it.next());
		}
		rval.append("]");

		return rval.toString();
	}
}
