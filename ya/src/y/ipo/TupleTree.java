package y.ipo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * ������ ������������� ��������
 */
public class TupleTree {
	List<Parameter> params;

	TupleTreeNode root;

	TupleTreeNode head;

	// ������ �� ������������ ��������
	int firstMissingValue;

	// ��� ������������ �� ��������
	int currValue;
	TupleTreeNode currNode;

	public TupleTree(List<Parameter> params) {
		this.params = params;
		init();
	}

	private void init() {
		root = new TupleTreeNode(-1,
				((Parameter) params.get(0)).getDomainSize());

		List<TupleTreeNode> currLevel = new ArrayList<TupleTreeNode>(1);
		currLevel.add(root);

		for (int level = 0; level < params.size(); level++) {
			Parameter param = (Parameter) params.get(level);
			if (level == params.size() - 1) {
				head = new TupleTreeNode(-1, param.getDomainSize());

				TupleTreeNode sibling = null;

				for (int j = 0; j < currLevel.size(); j++) {
					TupleTreeNode it = (TupleTreeNode) currLevel.get(j);
					for (int i = 0; i < param.getDomainSize(); i++) {
						if (sibling == null) {
							// ������ ������
							head.setKid(i, it);
							// �������� ������
							it.setKid(2 * i + 1, head);
						} else {
							// ������ ������
							sibling.setKid(2 * i, it);
							// �������� ������
							it.setKid(2 * i + 1, sibling);
						}
					}
					sibling = it;
				}
			} else {
				int numOfKids = ((Parameter) params.get(level + 1))
						.getDomainSize();
				// ������ �������� ������
				List<TupleTreeNode> nextLevel = new ArrayList<TupleTreeNode>(
						currLevel.size() * param.getDomainSize());

				for (int j = 0; j < currLevel.size(); j++) {
					TupleTreeNode it = (TupleTreeNode) currLevel.get(j);
					for (int i = 0; i < param.getDomainSize(); i++) {
						TupleTreeNode node = null;
						if (level < params.size() - 2) {
							node = new TupleTreeNode(i, numOfKids);
						} else {
							// ������ ������� �������� ������ ���������� �������
							node = new TupleTreeNode(i, 2 * numOfKids);
						}
						it.setKid(i, node);
						node.setDad(it);

						nextLevel.add(node);
					}
				}

				currLevel = nextLevel;
			}
		}

		firstMissingValue = 0;
		currValue = 0;
		currNode = head.getKid(currValue + 1);
	}

	/**
	 * ����� ������� � ������ ������ ������ ����� �� �� ������� � � ��� ��
	 * ������� ��� � � ������
	 */
	public TupleTreeNode lookup(Tuple tuple) {
		TupleTreeNode rval = null;

		if (tuple.getNumOfPairs() == 1) {
			rval = root.getKid(tuple.getPair(0).value);
		} else {
			LinkedList<TupleTreeNode> nodes = new LinkedList<TupleTreeNode>();
			nodes.add(root);
			int numOfPairs = tuple.getNumOfPairs();
			// ��������� ���� �� �������
			for (int i = 0; i < numOfPairs - 1; i++) {
				int value = tuple.getPair(i).value;

				// ���������� ����� �� ������� ������
				int numOfNodes = nodes.size();
				for (int j = 0; j < numOfNodes; j++) {
					TupleTreeNode node = (TupleTreeNode) nodes.removeFirst();
					if (value >= 0) {
						nodes.add(node.getKid(value));
					}
				}
			}

			int lastIndex = tuple.getPair(tuple.getNumOfPairs() - 1).value;

			// ��������� ������ �� ������
			for (int i = 0; i < nodes.size(); i++) {
				TupleTreeNode node = (TupleTreeNode) nodes.get(i);
				if (node.getKid(2 * lastIndex) != null
						|| node.getKid(2 * lastIndex + 1) != null) {
					rval = node;
					break;
				}
			}
		}

		return rval;
	}

	/**
	 * �������� ������ ��� ��������
	 */
	public boolean setCovered(Tuple tuple) {
		boolean rval = false;

		if (tuple.getNumOfPairs() == 1) {
			head.setKid(tuple.getPair(0).value, null);
			return true;
		}

		TupleTreeNode node = lookup(tuple);
		if (node != null) {
			int lastIndex = tuple.getPair(tuple.getNumOfPairs() - 1).value;
			TupleTreeNode succ = node.getKid(2 * lastIndex);
			TupleTreeNode prev = node.getKid(2 * lastIndex + 1);
			if (prev == head) {
				prev.setKid(lastIndex, succ);
			} else {
				prev.setKid(2 * lastIndex, succ);
			}
			if (succ != null) {
				succ.setKid(2 * lastIndex + 1, prev);
			}
			node.setKid(2 * lastIndex, null);
			node.setKid(2 * lastIndex + 1, null);
			rval = true;
		}
		return rval;
	}

	/**
	 * �������� ��������� ���������� ������
	 */
	public Tuple getNextMissingTuple() {
		Tuple rval = null;
		Parameter lastParam = (Parameter) params.get(params.size() - 1);
		while (firstMissingValue < lastParam.getDomainSize()) {
			TupleTreeNode node = head.getKid(firstMissingValue);
			if (node == null) {
				firstMissingValue++;
			} else {
				rval = new Tuple();
				if (root != head) {
					for (int i = params.size() - 2; i >= 0; i--) {
						Parameter param = (Parameter) params.get(i);
						PVPair pair = new PVPair(param, node.getValue());
						rval.addPair(pair);

						// ��������� �����
						node = node.getDad();
					}
				}
				rval.addPair(new PVPair(lastParam, firstMissingValue));
				break;
			}
		}
		return rval;
	}

	/**
	 * ������� ��������� ���������� ������
	 */
	public void coverNextMissingTuple() {
		TupleTreeNode curr = head.getKid(firstMissingValue);
		if (curr.getValue() == -1) {
			head.setKid(firstMissingValue, null);
			firstMissingValue++;
		} else {
			TupleTreeNode next = curr.getKid(2 * firstMissingValue);
			head.setKid(firstMissingValue, next);
			curr.setKid(2 * firstMissingValue, null);
			curr.setKid(2 * firstMissingValue + 1, null);
			if (next != null) {
				next.setKid(2 * firstMissingValue + 1, head);
			}
			// ��������� - ������� �� ��� ��������� �������
			if (head.getKid(firstMissingValue) == null) {
				firstMissingValue++;
			}
		}
	}

	/**
	 * ������� �� ������� ������
	 */
	public Tuple isCovered() {
		Tuple rval = null;
		if (head != null) {
			rval = getNextMissingTuple();
		}
		return rval;
	}

	public void initializeIterator() {
		currValue = 0;
		currNode = head.getKid(currValue);
	}

	public Tuple nextTuple() {
		Tuple rval = null;
		Parameter lastParam = (Parameter) params.get(params.size() - 1);
		while (currNode == null && currValue < lastParam.getDomainSize() - 1) {
			currValue++;
			currNode = head.getKid(currValue);
		}

		if (currNode != null) {
			TupleTreeNode node = currNode;
			rval = new Tuple();
			if (root != head) {
				for (int i = params.size() - 2; i >= 0; i--) {
					Parameter param = (Parameter) params.get(i);
					PVPair pair = new PVPair(param, node.getValue());
					rval.addPair(pair);

					// ����� �� ������
					node = node.getDad();
				}
			}

			// �������� ������ � ���������� ���������
			rval.addPair(new PVPair(lastParam, currValue));

			if (currNode.getValue() == -1) {
				currValue++;
				if (currValue < lastParam.getDomainSize()) {
					currNode = head.getKid(currValue);
				} else {
					currNode = null;
				}
			} else {
				currNode = currNode.getKid(2 * currValue);
			}
		}

		return rval;
	}

	public String toString() {
		StringBuffer rval = new StringBuffer();

		for (int i = 0; i < params.size(); i++) {
			Parameter param = (Parameter) params.get(i);
			rval.append(param);
		}
		rval.append("\n");

		List<TupleTreeNode> que = new LinkedList<TupleTreeNode>();
		que.add(root);

		Parameter lastParam = (Parameter) params.get(params.size() - 1);
		for (int i = 0; i < lastParam.getDomainSize(); i++) {
			rval.append("���������� ������� �� ��������� (" + i + "):");
			if (root == head) {
				if (head.getKid(i) != null) {
					rval.append(i + " ");
				}
			} else {
				TupleTreeNode node = head;
				while (node != null) {
					rval.append(node.getValue()).append(" ");
					if (node == head) {
						node = node.getKid(i);
					} else {
						node = node.getKid(2 * i);
					}
				}
				rval.append("\n");
			}
		}

		return rval.toString();
	}
}
