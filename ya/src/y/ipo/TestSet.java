package y.ipo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * �������� �����. ������ �������, ��� ������ ��� �������� �����, ������� -
 * �������� �����
 */
public class TestSet {
	public static final int DONT_CARE = -2;

	private List<Parameter> params;
	private List<Integer[]> matrix;

	public TestSet() {
		matrix = new ArrayList<Integer[]>();
	}

	public TestSet(List<Parameter> params) {
		this.params = params;
		matrix = new ArrayList<Integer[]>();
	}

	public void setParams(List<Parameter> params) {
		this.params = params;
	}

	public List<Parameter> getParams() {
		return params;
	}

	public int getNumOfParams() {
		return params.size();
	}

	public Parameter getParam(int index) {
		return (Parameter) params.get(index);
	}

	public void addMatrix(List<Integer[]> matrix) {
		for (int j = 0; j < matrix.size(); j++) {
			Integer[] row = (Integer[]) matrix.get(j);
			int numOfColumns = row.length < getNumOfParams() ? row.length
					: getNumOfParams();
			Integer[] test = new Integer[getNumOfParams()];
			Arrays.fill(test, DONT_CARE);
			for (int i = 0; i < numOfColumns; i++) {
				test[i] = row[i];
			}
			this.matrix.add(test);
		}
	}

	public List<Integer[]> getMatrix() {
		return matrix;
	}

	public int getNumOfTests() {
		return matrix.size();
	}

	public Integer[] getTest(int index) {
		return (Integer[]) matrix.get(index);
	}

	public int getValue(int row, int column) {
		return ((Integer[]) matrix.get(row))[column];
	}

	public void setValue(int row, int column, int value) {
		((Integer[]) matrix.get(row))[column] = value;
	}

	/**
	 * �������� - ��������� �� �������� ����� ������
	 */
	public boolean isCompatible(int row, Tuple tuple) {
		boolean rval = true;
		int numOfPairs = tuple.getNumOfPairs();
		for (int i = 0; i < numOfPairs; i++) {
			PVPair pair = tuple.getPair(i);
			int column = pair.param.getID();
			if (((Integer[]) matrix.get(row))[column] != DONT_CARE
					&& ((Integer[]) matrix.get(row))[column] != pair.value) {
				rval = false;
				break;
			}
		}
		return rval;
	}

	/**
	 * ������ �������� ����� ���, ����� �� �������� ������
	 */
	public void cover(int row, Tuple tuple) {
		int numOfPairs = tuple.getNumOfPairs();
		for (int i = 0; i < numOfPairs; i++) {
			PVPair pair = tuple.getPair(i);
			int column = pair.param.getID();
			((Integer[]) matrix.get(row))[column] = pair.value;
		}
	}

	/**
	 * ��������� ����� �������� �����, ����������� ������, ��������� -2
	 */
	public void addNewTest(Tuple tuple) {
		Integer[] test = new Integer[getNumOfParams()];
		Arrays.fill(test, DONT_CARE);
		matrix.add(test);
		cover(matrix.size() - 1, tuple);
	}

	public Integer[] createNewTest(Tuple tuple) {
		Integer[] rval = new Integer[getNumOfParams()];
		Arrays.fill(rval, DONT_CARE);
		int numOfPairs = tuple.getNumOfPairs();
		for (int i = 0; i < numOfPairs; i++) {
			PVPair pair = tuple.getPair(i);
			int column = pair.param.getID();
			rval[column] = pair.value;
		}
		return rval;
	}

	public void add(Integer[] test) {
		matrix.add(test);
	}

	public int getNumOfDifferences(int row1, int row2, int column) {
		int rval = 0;
		for (int i = 0; i < column; i++) {
			if (((Integer[]) matrix.get(row1))[i] != ((Integer[]) matrix
					.get(row2))[i]) {
				rval++;
			}
		}
		return rval;
	}

	public int[] clone(int row) {
		int[] rval = new int[getNumOfParams()];
		for (int i = 0; i < rval.length; i++) {
			rval[i] = getValue(row, i);
		}
		return rval;
	}

	public List<String> toStringList(int b) {
		List<String> result = new ArrayList<String>(b);
		for (int i = 0; i < b; i++) {
			if (b > matrix.size())
				b = matrix.size();
			StringBuffer rval = new StringBuffer("");
			for (int j = 0; j < ((Integer[]) matrix.get(i)).length; j++) {
				if (j > 0) {
					rval.append(" ");
				}
				rval.append(((Integer[]) matrix.get(i))[j]);
			}
			result.add(rval.toString());
		}

		return result;
	}

	public String toString() {
		StringBuffer rval = new StringBuffer();
		for (int i = 0; i < matrix.size(); i++) {
			for (int j = 0; j < ((Integer[]) matrix.get(i)).length; j++) {
				if (j > 0) {
					rval.append(" ");
				}
				rval.append(((Integer[]) matrix.get(i))[j]);
			}
			rval.append("\n");
		}

		return rval.toString();
	}
}
