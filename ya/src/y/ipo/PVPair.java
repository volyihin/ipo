package y.ipo;

/**
 * Параметр значение
 */
public class PVPair {

	public Parameter param;

	public int value;

	public PVPair(Parameter param, int value) {
		this.param = param;
		this.value = value;
	}

	public boolean equals(PVPair other) {
		return param.getID() == other.param.getID() && value == other.value;
	}

	public String toString() {
		return "(" + param.getName() + "," + param.getValue(value) + ")";
	}
}
