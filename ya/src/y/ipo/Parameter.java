package y.ipo;

import java.util.ArrayList;
import java.util.List;

public class Parameter {
	private int id;
	private String name;
	private List<String> values;

	public Parameter(String name) {
		this.name = name;

		values = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return id;
	}

	public void addValue(String value) {
		values.add(value);
	}

	public String getValue(int index) {
		return (String) values.get(index);
	}

	public int getDomainSize() {
		return values.size();
	}

	public String toString() {
		StringBuffer rval = new StringBuffer();
		rval.append(name + ": [");
		for (int i = 0; i < values.size(); i++) {
			if (i > 0) {
				rval.append(", ");
			}
			rval.append(values.get(i));
		}
		rval.append("]\n");

		return rval.toString();
	}

	public static List<Parameter> generateParamList(Integer[] a, int n) {
		List<Parameter> result = new ArrayList<Parameter>(n);
		for (int i = 0; i < n; i++) {
			String name = String.valueOf((char) ((int) 'a' + i));
			Parameter parameter = new Parameter(name);
			for (int j = 0; j < a[i]; j++) {
				parameter.addValue(name + j);
			}
			result.add(parameter);
		}
		return result;
	}

}
