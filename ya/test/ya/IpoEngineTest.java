package ya;

import junit.framework.Assert;

import org.junit.Test;

import com.sun.istack.internal.logging.Logger;

import y.ipo.IpoEngine;
import y.ipo.Parameter;

public class IpoEngineTest {

	private final static Logger logger = Logger.getLogger(IpoEngineTest.class);

	@Test
	public void testA() {
		Integer[] a = new Integer[] { 2, 3, 4, 5, 6, 7 };
		int n = 6;
		int b = 10;

		IpoEngine engine = new IpoEngine(Parameter.generateParamList(a, n), b);
		engine.build();

		logger.info(engine.getTestSet().toString());
		Assert.assertEquals(engine.getTestSet().toStringList(b).size(), b);

	}

	@Test
	public void testB() {
		Integer[] a = new Integer[] { 2, 2, 2, 3, 10, 7 };
		int n = 6;
		int b = 30;

		IpoEngine engine = new IpoEngine(Parameter.generateParamList(a, n), b);
		engine.build();

		logger.info(engine.getTestSet().toString());
		Assert.assertEquals(engine.getTestSet().toStringList(b).size(), b);

	}

	@Test
	public void testC() {
		Integer[] a = new Integer[] { 2, 2, 2, 3, 10, 7, 10, 10, 10, 10, 10, 10 };
		int n = 12;
		int b = 30;

		IpoEngine engine = new IpoEngine(Parameter.generateParamList(a, n), b);
		engine.build();

		logger.info(engine.getTestSet().toString());
		Assert.assertEquals(engine.getTestSet().toStringList(b).size(), b);

	}
}
